<?php

namespace Tests\Feature;

use App\Models\Author;
use App\Models\Book;
use App\Models\Category;
use App\Models\User;
use Tests\TestCase;

class BookUserTest extends TestCase
{

    public $user;
    public $category;
    public $book;
    public $author;
    public $taken_book;


    protected function setUp(): void
    {
        parent::setUp();
        $this->author = Author::factory()->create();
        $this->user = User::factory()->create();
        $this->category = Category::factory()->create();
        $this->taken_book = Book::factory()->for($this->category)->for($this->author)->create();
        $this->taken_book->update();
        $this->book = Book::factory()->for($this->category)->for($this->author)->create();

    }

    /**
     *
     * @return void
     */
    public function test_not_auth_user_can_not_see_show_action()
    {
        $response = $this->get(route('book_user.show', ['book_user' => $this->book->id]));
        $response->assertRedirect();
    }

    /**
     *
     * @return void
     */
    public function test_not_auth_user_can_see_show_action()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('book_user.show', ['book_user' => $this->book->id]));
        $response->assertStatus(200);
    }


    /**
     *
     * @return void
     */
    public function test_not_auth_user_can_not_see_index_action()
    {
        $response = $this->get(route('book_user.index'));
        $response->assertRedirect();
    }

    /**
     *
     * @return void
     */
    public function test_auth_user_can_see_index_action()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('book_user.index'));
        $response->assertStatus(200);
    }
}
