<?php

namespace Tests\Feature;

use App\Models\Author;
use App\Models\Book;
use App\Models\Category;
use App\Models\User;
use Tests\TestCase;

class BooksTest extends TestCase
{

    public $user;
    public $category;
    public $book;
    public $author;
    public $taken_book;


    protected function setUp(): void
    {
        parent::setUp();
        $this->author = Author::factory()->create();
        $this->user = User::factory()->create();
        $this->category = Category::factory()->create();
        $this->taken_book = Book::factory()->for($this->category)->for($this->author)->create();
        $this->taken_book->update();
        $this->book = Book::factory()->for($this->category)->for($this->author)->create();

    }

    /**
     *
     * @return void
     */
    public function test_not_auth_user_can_not_take_the_book()
    {
        $response = $this->get(route('books.update', ['book' => $this->book]));
        $response->assertRedirect();
    }

    /**
     *
     * @return void
     */
    public function test_auth_user_can_take_the_book()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('books.update', ['book' => $this->book]));
        $response->assertStatus(200);
    }

}
