<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\User;
use Tests\TestCase;

class CategoriesTest extends TestCase
{

    public $user;
    public $category;


    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->category = Category::factory()->create();
    }

    /**
     *
     * @return void
     */
    public function test_not_auth_user_can_not_see()
    {
        $response = $this->get('/categories');
        $response->assertRedirect();
    }

    /**
     *
     * @return void
     */
    public function test_auth_user_can_see()
    {
        $this->actingAs($this->user);
        $response = $this->get('/categories');
        $response->assertStatus(200);
    }

    /**
     *
     * @return void
     */
    public function test_categories_index_have_parameters()
    {
        $this->actingAs($this->user);
        $response = $this->get('/categories');
        $response->assertStatus(200);
        $response->assertViewHas('categories');
    }


    /**
     *
     * @return void
     */
    public function test_categories_show_have_parameters()
    {
        $this->actingAs($this->user);
        $response = $this->get(route('categories.show', ['category' => $this->category]));
        $response->assertStatus(200);
        $response->assertViewHas('categories');
        $response->assertSee($this->category->category);
    }


    /**
     *
     * @return void
     */
    public function test_not_auth_user_can_not_get_show()
    {
        $response = $this->get(route('categories.show', ['category' => $this->category]));
        $response->assertRedirect();
    }
}
