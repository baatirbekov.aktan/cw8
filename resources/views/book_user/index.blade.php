@extends('layouts.app')

@section('content')


    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Book</th>
            <th scope="col">Return date</th>
            <th scope="col">Status</th>
        </tr>
        </thead>
        <tbody>
        @foreach($books as $book)
            <tr>
                <th scope="row">{{$book->id}}</th>
                <td>{{$book->book_name}}</td>
                <td>{{$book->return_at}}</td>
                <td>{{$book->status}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
