@extends('layouts.app')

@section('content')
    <div class="m-3">
        <form action="{{route('books.update', ['book' => $book])}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-row">
                <div class="col">
                    <label for="library_card_number">Card number</label>
                    <input id="library_card_number" name="library_card_number" type="text" class="form-control"
                           value="{{session()->get('library_card_number')}}">
                </div>
                <div class="col">
                    <label for="return_at">Return Date</label>
                    <input id="return_at" name="return_at" type="date" class="form-control" placeholder="Last name">
                </div>
            </div>
            <div class="form-row mt-3">
                <div class="col">
                    <label for="book_id">Book</label>
                    <input disabled id="book_id" type="text" class="form-control" value="{{$book->name}}">
                </div>
                <div class="col">
                    <label for="author_id">Author</label>
                    <input disabled id="author_id" type="text" class="form-control" value="{{$book->author->name}}">
                </div>
            </div>
            <button class="mt-3 btn btn-outline-success">Save</button>
        </form>
    </div>
@endsection
