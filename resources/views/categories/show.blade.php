@extends('layouts.app')

@section('categories')
    <ul class="list-group w-100">
        @foreach($categories as $book)
            <li class="list-group-item">
            <a href="{{route('categories.show', ['category' => $book])}}">{{$book->category}}</a>
            </li>
        @endforeach
    </ul>
@endsection

@section('content')
    <div class="row row-cols-1 row-cols-md-3">
        @foreach($category->books as $book)
            <div class="col mb-4">
                <div class="card h-100">
                    <img src="{{asset('/storage/' . $book->picture)}}" class="card-img-top" alt="{{$book->picture}}">
                    <div class="card-body">
                        <h5 class="card-title">{{$book->name}}
                        </h5>
                        <p class="card-text">{{$book->description}}</p>
                        <p>{{$book->author->name}}</p>
                    </div>
                    <div class="card-footer">
                        @if($book->active)
                                <a class="btn btn-outline-success" href="{{route('book_user.show', ['book_user' => $book])}}">Add</a>
                        @else
                          Дата возврата:  {{$book->return_at}}
                        @endif
                    </div>
                </div>
            </div>

        @endforeach
    </div>

    <div>
        {{$category->books->links('pagination::bootstrap-4')}}
    </div>

@endsection

