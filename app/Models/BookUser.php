<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class BookUser extends Pivot
{
    use HasFactory;

    protected $fillable = ['user_id', 'book_id', 'return_at', 'status', 'book_name'];


    const EXPECTED = 'expected';

    const RETURNED = 'returned';

    const EXPIRED = 'expired';

    /**
     * @return string
     */
    public function getExpected()
    {
        return self::EXPECTED;
    }

    /**
     * @return string
     */
    public function getExpired()
    {
        return self::EXPIRED;
    }

    /**
     * @return string
     */
    public function getReturned()
    {
        return self::RETURNED;
    }
}
