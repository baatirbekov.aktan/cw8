<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class CategoryController extends Controller
{
    /**
     * CategoryController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $categories = Category::all();
        $category = Category::first();
        $category->setRelation('books', $category->books()->paginate(3));
        return view('categories.show', ['category' => $category], compact('categories'));
    }

    /**
     * Display the specified resource.
     *
     * @param Category $category
     * @return Application|Factory|View
     */
    public function show(Category $category)
    {
        $categories = Category::all();
        $category->setRelation('books', $category->books()->paginate(3));
        return view('categories.show', ['category' => $category], compact('categories'));
    }
}
