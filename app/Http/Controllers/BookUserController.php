<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\BookUser;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class BookUserController extends Controller
{

    /**
     * BookUserController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $books = BookUser::all()->where('user_id', Auth::id());
        foreach ($books as $book) {
            if ($book->returned) {
                $book->status = $book->getReturned();
            } else {
                if ($book->return_at < date("Y-m-d")) {
                    $book->status = $book->getExpired();
                }
            }
        }
        return view('book_user.index', compact('books'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $int
     * @return Application|Factory|View
     */
    public function show(int $int)
    {
        $book = Book::find($int);
        return view('book_user.create', compact('book'));
    }
}
