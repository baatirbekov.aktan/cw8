<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\BookUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{

    /**
     * BookController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Book $book
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Book $book)
    {
        if ($book->active) {
            if ($request->session()->get('library_card_number') == $request->get('library_card_number')) {
                if ($request->input('return_at') > date("Y-m-d")) {
                    $book->active = false;
                    $book->return_at = $request->input('return_at');
                    $book->update();
                    $book_user = new BookUser();
                    $book_user->user_id = Auth::id();
                    $book_user->book_id = $book->id;
                    $book_user->book_name = $book->name;
                    $book_user->status = $book_user->getExpected();
                    $book_user->return_at = $request->input('return_at');
                    $book_user->save();
                    return redirect()->route('categories.show', ['category' => $book->category])->with('success', 'Successfully added');
                }
                return redirect()->back()->with('error', 'You have to choose valid date');
            } else {
                return redirect()->back()->with('error', 'You entered invalid card number!');
            }
        } else {
            return redirect()->back()->with('error', 'This book is already taken');
        }
    }
}
