<?php

namespace Database\Seeders;

use App\Models\Author;
use App\Models\Book;
use App\Models\Category;
use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $authors = Author::all();
        $categories = Category::all();
        foreach ($authors as $author) {
            foreach ($categories as $category) {
                Book::factory()->for($author)->for($category)->count(4)->create();
            }
        }
    }
}
