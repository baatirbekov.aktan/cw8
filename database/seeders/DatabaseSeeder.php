<?php

namespace Database\Seeders;

use App\Models\Book;
use App\Models\BookUser;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();

        $this->call(AuthorSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(BookSeeder::class);

        $books = Book::all();
        $users = User::all();


            for ($i = 0; $i < 100; $i++) {
                $book = $books->random();
                $book->active = false;
                $book->return_at = "2021-12-12";
                $book->save();
                $application = new BookUser();
                $application->user_id = $users->random()->id;
                $application->book_id = $book->id;
                $application->return_at = "2021-12-12";
                $application->book_name = $book->name;
                $application->status = $application->getExpected();
                $application->save();
            }

    }
}
